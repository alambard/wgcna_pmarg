# wgcna_pmarg

WGCNA and other complementary analyses of two oyster species: _Pincatada margaritifera_ and _Crassostrea Gigas_ . From gene expression matrices.


# Dependencies

* R packages 
    * for WGCNA/module preservation : assertthat, scales, WGCNA, readr
    * for KOGMWU : KOGMWU, pheatmap, RcolorBrewer


# Tutorials 

https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/Rpackages/WGCNA/Tutorials/
https://horvath.genetics.ucla.edu/html/CoexpressionNetwork/ModulePreservation/Tutorials/
