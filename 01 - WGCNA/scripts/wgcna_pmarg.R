#!/usr/bin/Rscript

################ WGCNA RNA-seq Analysis project Aiaiai ############

ls()
rm(list=ls())
ls()


# 1- Load packages -----
library("assertthat")
library("scales")
library("WGCNA")
setwd("/home/alambard/Desktop/cgi_pmarg/WGCNA/R/Pmarg/")
# The following setting is important, do not omit.
options(stringsAsFactors = FALSE);


# 2- Loading expression data -----
#Read in the expr data set
logcpm_test<-read.table("matrix_pmarg", header=T ,check.names = F)
head(logcpm_test)
transcriptname<-rownames(logcpm_test)

temp<-read.table("data_traits.txt",sep="\t", header=T,na.strings="NA",check.names = F)
vector<-temp$Ind

logcpm_test<-logcpm_test[, names(logcpm_test) %in% vector]
colnames(logcpm_test)
# Take a quick look at what is in the data set:
dim(logcpm_test)
head(logcpm_test)

datExpr0 = as.data.frame(t(logcpm_test))
head(datExpr0)

gsg = goodSamplesGenes(datExpr0, verbose = 3);

gsg$allOK



if (!gsg$allOK)
{
  # Optionally, print the gene and sample names that were removed:
  if (sum(!gsg$goodGenes)>0) 
    printFlush(paste("Removing genes:", paste(names(datExpr0)[!gsg$goodGenes], collapse = ", ")));
  if (sum(!gsg$goodSamples)>0) 
    printFlush(paste("Removing samples:", paste(rownames(datExpr0)[!gsg$goodSamples], collapse = ", ")));
  # Remove the offending genes and samples from the data:
  datExpr0 = datExpr0[gsg$goodSamples, gsg$goodGenes]
}


sampleTree = hclust(dist(datExpr0), method = "average");
# Plot the sample tree: Open a graphic output window of size 12 by 9 inches
# The user should change the dimensions if the window is too large or too small.
sizeGrWindow(12,9)
#pdf(file = "Plots/sampleClustering.pdf", width = 12, height = 9);
par(cex = 0.6);
par(mar = c(0,4,2,0))
plot(sampleTree, main = "Sample clustering to detect outliers", sub="", xlab="", cex.lab = 1.5, 
     cex.axis = 1.5, cex.main = 2)

# 3- Check outliers -----


# Plot a line to show the cut
abline(h = 450, col = "red");
# Determine cluster under the line
clust = cutreeStatic(sampleTree, cutHeight = 450, minSize = 10)
table(clust)
# clust 1 contains the samples we want to keep.
keepSamples = (clust==1)
datExpr = datExpr0[keepSamples, ]
nGenes = ncol(datExpr)
nSamples = nrow(datExpr)
variancedatExpr=as.vector(apply(as.matrix(datExpr),2,var, na.rm=T))
no.presentdatExpr=as.vector(apply(!is.na(as.matrix(datExpr)),2, sum) )
# Another way of summarizing the number of pressent entries
table(no.presentdatExpr)

# Keep only genes whose variance is non-zero and have at least 4 present entries
KeepGenes= variancedatExpr>0.5 # TEST for 0.05
table(KeepGenes)
datExpr=datExpr[, KeepGenes]

name_datExpr <-colnames(datExpr)
write.table(name_datExpr,file="genes.wgcna.kept.meanvar.0.05.txt",quote=F)
# 4- Add traits ----

allTraits = read.table("data_traits.txt",header=T,check.names=F,na.strings="NA");
names(allTraits)

# Form a data frame analogous to expression data that will hold the clinical traits.

femaleSamples = rownames(datExpr);
traitRows = match(femaleSamples, allTraits$Ind);
datTraits = allTraits[traitRows, -1];
rownames(datTraits) = allTraits[traitRows, 1];
str(datTraits)

collectGarbage();



summary(temp)
# Re-cluster samples
sampleTree2 = hclust(dist(datExpr), method = "average")
# Convert traits to a color representation: white means low, red means high, grey means missing entry
traitColors = numbers2colors(datTraits,signed= TRUE);
# Plot the sample dendrogram and the colors underneath.
pdf("dendo_heatmap_subset.pdf",width=12,height=9)
par(mar=c(1, 10, 1, 1))
plotDendroAndColors(sampleTree2, traitColors,
                    groupLabels = names(datTraits), 
                    main = "Sample dendrogram and trait heatmap")

dev.off()
summary(datTraits)


save(datExpr, datTraits, file = "dataInput_aiaiai_var0.05.Rda")



# 5- Soft-threshold definition -----

#se,tting is important, do not omit.
options(stringsAsFactors = FALSE);
# Allow multi-threading within WGCNA. At present this call is necessary.
# Any error here may be ignored but you may want to update WGCNA if you see one.
# Caution: skip this line if you run RStudio or other third-party R environments.
# See note above.
#enableWGCNAThreads()
# Load the data saved in the first part
lnames = load(file = "dataInput_aiaiai_var0.05.Rda");
#The variable lnames contains the names of loaded variables.
lnames


# Choose a set of soft-thresholding powers
powers = c(1:20)
# Call the network topology analysis function
sft = pickSoftThreshold(datExpr, powerVector = powers, verbose = 5,networkType="signed")
# Plot the results:
#load("sft_signed_cell_culture_subset_REVISED_var0.05.Rda")
#pdf(file = "wgcna_mean_connectivity_aiaiai.pdf", width = 20, height = 20)
sizeGrWindow(9, 5)
par(mfrow = c(1,2));
cex1 = 0.9;
# Scale-free topology fit index as a function of the soft-thresholding power
plot(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2],
     xlab="Soft Threshold (power)",ylab="Scale Free Topology Model Fit,signed R^2",type="n",
     main = paste("Scale independence"));
text(sft$fitIndices[,1], -sign(sft$fitIndices[,3])*sft$fitIndices[,2],
     labels=powers,cex=cex1,col="red");
# this line corresponds to using an R^2 cut-off of h
abline(h=0.9,col="red")
# Mean connectivity as a function of the soft-thresholding power
plot(sft$fitIndices[,1], sft$fitIndices[,5],
     xlab="Soft Threshold (power)",ylab="Mean Connectivity", type="n",
     main = paste("Mean connectivity"))
text(sft$fitIndices[,1], sft$fitIndices[,5], labels=powers, cex=cex1,col="red")
dev.off()

save(sft,file="sft_signed_aiaiai_var0.05.Rda")
View(sft$fitIndices)


# 6- Module construction step-by-step (to run on Datarmor) -----

#setting is important, do not omit.
options(stringsAsFactors = FALSE);
# Allow multi-threading within WGCNA. At present this call is necessary.
# Any error here may be ignored but you may want to update WGCNA if you see one.
# Caution: skip this line if you run RStudio or other third-party R environments.
# See note above.
#enableWGCNAThreads()
# Load the data saved in the first part
lnames = load(file = "dataInput_aiaiai_var0.05.Rda");
#The variable lnames contains the names of loaded variables.
lnames


load("dataInput_aiaiai_var0.05.Rda")
load("sft_signed_aiaiai_var0.05.Rda")


softPower = 12; #91 % 
adjacency = adjacency(datExpr, power = softPower,type="signed");
save(adjacency,file="adjacency_host_REVISED_var0.05.Rda")

TOM = TOMsimilarity(adjacency,TOMType = "signed");
load("TOM_REVISED_var0.05.Rda")
dissTOM = 1-TOM

#save(dissTOM,file="disTOM_REVISED_var0.05.Rda")
#save(TOM,file="TOM_REVISED_var0.05.Rda")
message("dissimilarity done")


# Call the hierarchical clustering function
geneTree = hclust(as.dist(dissTOM), method = "average");
# Plot the resulting clustering tree (dendrogram)
pdf("genetree_subset.pdf",width=12,height=9)
plot(geneTree, xlab="", sub="", main = "Gene clustering on TOM-based dissimilarity",
     labels = FALSE, hang = 0.04);
dev.off()


# We like large modules, so we set the minimum module size relatively high:
minModuleSize = 50;
# Module identification using dynamic tree cut:
dynamicMods = cutreeDynamic(dendro = geneTree, distM = dissTOM,
                            deepSplit = 2, pamRespectsDendro = FALSE,
                            minClusterSize = minModuleSize);
table(dynamicMods)


# Convert numeric lables into colors
dynamicColors = labels2colors(dynamicMods)
table(dynamicColors)
# Plot the dendrogram and colors underneath
#pdf("genetree_dyn_subset.pdf",width=8,height=6)
plotDendroAndColors(geneTree, dynamicColors, "Dynamic Tree Cut",
                    dendroLabels = FALSE, hang = 0.03,
                    addGuide = TRUE, guideHang = 0.05,
                    main = "Gene dendrogram and module colors")
dev.off()


# Calculate eigengenes
MEList = moduleEigengenes(datExpr, colors = dynamicColors)
MEs = MEList$eigengenes
# Calculate dissimilarity of module eigengenes
MEDiss = 1-cor(MEs);
# Cluster module eigengenes
METree = hclust(as.dist(MEDiss), method = "average");
# Plot the result
#pdf("cluster_modules_subset.pdf",width=8,height=6)
plot(METree, main = "Clustering of module eigengenes",
     xlab = "", sub = "")


MEDissThres = 0.25
# Plot the cut line into the dendrogram
abline(h=MEDissThres, col = "red")
# Call an automatic merging function
merge = mergeCloseModules(datExpr, dynamicColors, cutHeight = MEDissThres, verbose = 3)
# The merged module colors
mergedColors = merge$colors;
# Eigengenes of the new merged modules:
mergedMEs = merge$newMEs;
dev.off()

#pdf(file = "geneDendro-3_subset.pdf", wi = 9, he = 6)
plotDendroAndColors(geneTree, cbind(dynamicColors, mergedColors),
                    c("Dynamic Tree Cut", "Merged dynamic"),
                    dendroLabels = FALSE, hang = 0.03,
                    addGuide = TRUE, guideHang = 0.05)
dev.off()


# Rename to moduleColors
moduleColors = mergedColors
# Construct numerical labels corresponding to the colors
colorOrder = c("grey", standardColors(50));
moduleLabels = match(moduleColors, colorOrder)-1;
MEs = mergedMEs;
# Save module colors and labels for use in subsequent parts
save(MEs, moduleLabels, moduleColors, geneTree, file = "network_host_REVISED_var0.05.Rda")
exit

# 7- Visualize network ----------

# Load the expression and trait data saved in the first part
lnames = load(file = "dataInput_aiaiai_var0.05.Rda");
#The variable lnames contains the names of loaded variables.
lnames
# Load network data saved in the second part.
lnames = load(file = "networkConstruction_aiaiai.Rda");
lnames
nGenes = ncol(datExpr)
nSamples = nrow(datExpr)
# load SFT  ### not made in the proper package
lnames=load(file="sft_signed_aiaiai_var0.05.Rda")
lnames


# Define numbers of genes and samples
nGenes = ncol(datExpr);
nSamples = nrow(datExpr);
# Recalculate MEs with color labels
MEs0 = moduleEigengenes(datExpr, moduleColors)$eigengenes
write.table(MEs0,file="MEs.compute.aiaiai.txt",quote=F)

MEs = orderMEs(MEs0)
moduleTraitCor = cor(MEs, datTraits, use = "p");
moduleTraitPvalue = corPvalueStudent(moduleTraitCor, nSamples);
#moduleTraitPvalue[moduleTraitPvalue>0.049]<-0
#moduleTraitCor[moduleTraitCor > -0.6 & moduleTraitCor < 0.6]<-0
head(moduleTraitCor)
head(moduleTraitPvalue)

sizeGrWindow(25,15)


#tiff(file = "03_results/correlation_matrix_temp.tiff", width = 30, height = 25, units = "cm", res = 300)

# Will display correlations and their p-values
#moduleTraitPvalue[moduleTraitPvalue >0.049]=NA #test
textMatrix =  paste(signif(moduleTraitCor, 2), "\n(",
                    signif(moduleTraitPvalue, 1), ")", sep = "");



dim(textMatrix) = dim(moduleTraitCor)
par(mar = c(6, 10, 3, 3));
# Display the correlation values within a heatmap plot
labeledHeatmap(Matrix = moduleTraitCor,
               xLabels = names(datTraits),
               yLabels = names(MEs),
               ySymbols = names(MEs),
               colorLabels = FALSE,
               colors = blueWhiteRed(50),
               textAdj = c(0.5, 0.5),
               textMatrix = textMatrix,
               setStdMargins = FALSE,
               cex.text = 0.5,
               zlim = c(-1,1),
               main = paste("Module-trait relationships"))
#dev.off()


par(mar = c(1,1, 1 ,1));
library(corrplot)
moduleTraitPvalue[moduleTraitPvalue > 0.05]<-0
moduleTraitCor[moduleTraitCor > -0.31 & moduleTraitCor < 0.31]<-0
df_module<-as.data.frame(moduleTraitPvalue)
df_module<-as.data.frame(moduleTraitCor)
new_mat <- as.data.frame(moduleTraitCor[ rownames(moduleTraitCor) %in% c("MEblack","MEblue","MEbrown","MEcyan","MEgreen","MEgreenyellow","MEmagenta",
                                                                         "MEmidnightblue","MEpink","MEpurple","MEred","MEsalmon","MEtan",
                                                                         "MEturquoise","MEyellow"),])
row.names(new_mat)<-gsub("ME", "", row.names(new_mat)) 
row.names(new_mat)=c("black (443)","midnightlbue (83)", "yellow (525)","brown (1532)","magenta (249)","purple (153)","red (456)","cyan (93)",
                     "greenyellow (142)"," MEtan (120)", "turquoise (3639)")

mat_host<-as.matrix(new_mat[c(1,2:8)])
head(mat_host)


#pdf(file = "03_results/correlation_matrix_host_REVISED.pdf", width = 7, height = 4)
pdf(file = "Matrice_Pmarg_aiaiai_250821.pdf", width = 7, height = 4)
par(mar=c(2,5,6,5))
corrplothost<-corrplot(mat_host, method = "number",tl.col = "black",cl.pos = "n", col= colorRampPalette(c("darkblue","white", "darkred"))(100))
dev.off()


### Define cluster significance

pH= as.data.frame(datTraits$pHexpo);
names(pH) = "pH"
str(pH)
# names (colors) of the modules
modNames = substring(names(MEs), 3)

geneModuleMembership = as.data.frame(cor(datExpr, MEs, use = "p"));
str(geneModuleMembership)


MMPvalue = as.data.frame(corPvalueStudent(as.matrix(geneModuleMembership), nSamples));
str(MMPvalue)
names(geneModuleMembership) = paste("MM", modNames, sep="");
names(MMPvalue) = paste("p.MM", modNames, sep="");

geneTraitSignificance = as.data.frame(cor(datExpr, pH, use = "p"));
GSPvalue = as.data.frame(corPvalueStudent(as.matrix(geneTraitSignificance), nSamples));

names(geneTraitSignificance) = paste("GS.", names(pH), sep="");
names(GSPvalue) = paste("p.GS.", names(pH), sep="");


##significance by module
dev.off()
module = "green"
column = match(module, modNames);
moduleGenes = moduleColors==module;

pdf(file = "module_green.pdf", width = 15, height = 15)
plot<-verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
                         abs(geneTraitSignificance[moduleGenes, 1]),
                         xlab = paste("Module Membership in ", module, "module"),
                         ylab = "Gene significance for pH",
                         main = NULL, cex.lab = 1.2, cex.axis = 1.2, col = module)
plot + text(0.3, 0.65, "cor = 0.73\np-value < 0.001",cex = 1.5,font=2)
dev.off()



module = "red"
column = match(module, modNames);
moduleGenes = moduleColors==module;

pdf(file = "03_results/module_red.pdf", width = 15, height = 15)
plot<-verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
                         abs(geneTraitSignificance[moduleGenes, 1]),
                         xlab = paste("Module Membership in ", module, "module"),
                         ylab = "Gene significance for pH",
                         main = NULL, cex.lab = 1.2, cex.axis = 1.2, col = module)
plot + text(0.4, 0.65, "cor = 0.64\np-value < 0.001",cex = 1.5,font=2)
dev.off()

module = "yellow"
column = match(module, modNames);
moduleGenes = moduleColors==module;

pdf(file = "03_results/module_yellow.pdf", width = 15, height = 15)
plot<-verboseScatterplot(abs(geneModuleMembership[moduleGenes, column]),
                         abs(geneTraitSignificance[moduleGenes, 1]),
                         xlab = paste("Module Membership in ", module, "module"),
                         ylab = "Gene significance for pH",
                         main = NULL, cex.lab = 1.2, cex.axis = 1.2, col = module)
plot + text(0.3, 0.65, "cor = 0.58\np-value < 0.001",cex = 1.5,font=2)
dev.off()

# 8- Extract module info ------

annot = read.csv2(file = "annotation_genome_pmarg.110621.csv",sep=";",header=T);
dim(annot)
head(annot)
annot$name<-annot$name
#annot$name<-NULL
names(annot)
probes = names(datExpr)
probes
probes2annot = match(probes, annot$name)
# The following is the number or probes without annotation:
sum(is.na(probes2annot))
# Should return 0.

# Create the starting data frame
geneInfo0 = data.frame(geneID = probes,
                       geneSymbol = annot$Sp[probes2annot],
                       LocusLinkID = annot$ID[probes2annot],
                       moduleColor = moduleColors,
                       geneTraitSignificance,
                       GSPvalue)
# Order modules by their significance for temperature
modOrder = order(-abs(cor(MEs, pH, use = "p")));

# get relevant modules
# Get the corresponding Locuis Link IDs
allLLIDs = annot$name[probes2annot];
# $ Choose interesting modules
intModules = c("yellow","brown","purple")

for (module in intModules)
{
  # Select module probes
  modGenes = (moduleColors==module)
  # Get their entrez ID codes
  modLLIDs = allLLIDs[modGenes];
  # Write them into a file
  fileName = paste("module_", module, ".txt", sep="");
  write.table(as.data.frame(modLLIDs), file = fileName,
              row.names = FALSE, col.names = FALSE,quote=FALSE)
}

# A
# Add module membership information in the chosen order
for (mod in 1:ncol(geneModuleMembership))
{
  oldNames = names(geneInfo0)
  geneInfo0 = data.frame(geneInfo0, geneModuleMembership[, modOrder[mod]], 
                         MMPvalue[, modOrder[mod]]);
  names(geneInfo0) = c(oldNames, paste("MM.", modNames[modOrder[mod]], sep=""),
                       paste("p.MM.", modNames[modOrder[mod]], sep=""))
}

# Order the genes in the geneInfo variable first by module color, then by geneTraitSignificance
geneOrder = order(geneInfo0$moduleColor, -abs(geneInfo0$GS.pH));
geneInfo = geneInfo0[geneOrder, ]
write.csv(geneInfo, file = "geneinfo_pH_Pmarg.csv")
